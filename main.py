import redshift
import postgree
import config
import log

diretorio = config.Path['directory']
filename = 'drive_score.csv'

def gerararquivo(listar, arquivo):
    f = open(arquivo, 'w', encoding='utf-8')
    for item in listar:
        try:
            f.writelines( item
                                )
        except Exception as e:
            print('Erro =>' + str(e))
    f.close()

def main () :

    log.info('Inicio processo sincronimo Redshift com o Postgree Event Sumarizado ')
    listar = []

    try:

        listar= redshift.listareventosumarizado()

    except Exception as ex:
        log.error('Erro ao carregar lista de eventos do redshift' , ex)
        raise Exception(ex.__str__())

    if (listar.__len__() >  0 ) :

        try:
            log.info('Gerar arquivo com a lista de eventos por taxista => [{0}]'.format(listar.__len__()))
            gerararquivo(listar, diretorio + filename)
            log.info('Arquivo gerado com sucesso ')
        except Exception as ex:
            log.error('Erro ao gerar arquivo para carga na tabela temporaria ', ex)
            raise Exception(ex.__str__())

        try:
            log.info('Importar csv e fazer o sincronismo ')

            postgree.importcsv(diretorio + filename)

            log.info('Termnio ao importar csv e fazer o sincronismo ')

        except Exception as ex:
            log.error('Erro a carregar arquivo no Postgree para sincronizar ', ex)
            raise Exception(ex.__str__())

    else:
        log.info('Não localizado dados a serem sincronizados ')



def maindriver () :

    log.info('Inicio processo sincronimo Redshift com o Postgree Event Sumarizado ')
    listar = []

    try:

        listar= redshift.listareventosumarizadodriver()

    except Exception as ex:
        log.error('Erro ao carregar lista de eventos do redshift' , ex)
        raise Exception(ex.__str__())

    if (listar.__len__() >  0 ) :

        try:
            log.info('Gerar arquivo com a lista de eventos por taxista => [{0}]'.format(listar.__len__()))
            gerararquivo(listar, diretorio + filename)
            log.info('Arquivo gerado com sucesso ')
        except Exception as ex:
            log.error('Erro ao gerar arquivo para carga na tabela temporaria ', ex)
            raise Exception(ex.__str__())

        try:
            log.info('Importar csv e fazer o sincronismo ')

            postgree.importcsvdriverevent(diretorio + filename)

            log.info('Termnio ao importar csv e fazer o sincronismo ')

        except Exception as ex:
            log.error('Erro a carregar arquivo no Postgree para sincronizar ', ex)
            raise Exception(ex.__str__())

    else:
        log.info('Não localizado dados a serem sincronizados ')
if __name__ == '__main__':
    maindriver()
