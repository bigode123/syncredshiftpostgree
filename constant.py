sqleventosredshift ="select * from storyteller.viw_score_driver_ride_detail"
sqlupdateevent = """
   UPDATE 
base.driver_ride_score_detail x  
SET 
 driveracceptedride = r.driveracceptedride, 
 driverarrivedatpickup = r.driverarrivedatpickup,
 driverrescindedride  = r.driverrescindedride, 
 drideofferwithdrawn = r.drideofferwithdrawn, 
 driverrejectedride = r.driverrejectedride
from   base.driver_ride_score_detail_temp r where  x.driverid = r.driverid
        

"""
sqlinsertevent ="""
  insert into  base.driver_ride_score_detail ( driverid, driveracceptedride, driverarrivedatpickup, driverrescindedride, drideofferwithdrawn , driverrejectedride)
  select  r.driverid,  r.driveracceptedride,  r.driverarrivedatpickup,  r.driverrescindedride,  r.drideofferwithdrawn ,  r.driverrejectedride
  from  base.driver_ride_score_detail_temp r left join  base.driver_ride_score_detail x on r.driverid = x.driverid where x.driverid is null 
"""

sqldeleteevent = "delete from base.driver_ride_score_detail x  using  base.driver_ride_score_detail_temp r  where r.driverid is null  "


sqltruncatetemp = """ truncate table base.driver_ride_score_detail_temp   """

# Import from driver events
#


sqleventosredshiftdriver ="""select driverid ,  
event, key reason, count(*) qtde 
   from storyteller.event v left join storyteller.reason rd 
on v.eventid = rd.eventid   
where driverid is not null and timestamp >= DATEADD(Day ,-30, current_date) 
group by driverid, event,  key"""

sqltruncatetempdriver = """ truncate table base.driver_event_temp   """
sqldeleteeventdriver = """	delete from base.driver_event   using base.driver_event x
	LEFT JOIN base.driver_event r 	on 	x.driverid = r.driverid and x.event= r.event and x.reason= r.reason     where r.driverid is null  """

sqlupdateeventcriver = """
   UPDATE 
base.driver_event x  
SET 
  
 quantity = r.quantity
 
from   base.driver_event_temp r where  x.driverid = r.driverid and x.event = r.event and x.reason = r.reason


"""

sqlinserteventdriver ="""
  insert into  base.driver_event ( driverid, event, reason,quantity)
  select  r.driverid,  r.event,  r.reason,  r.quantity
  from  base.driver_event_temp r left join  base.driver_event x on r.driverid = x.driverid and x.event= r.event and x.reason= r.reason where x.driverid is null 
"""