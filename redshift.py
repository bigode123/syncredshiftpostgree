import config
import psycopg2
import constant

conn_string = "dbname='" + config.RedshiftWappa['database'] + "' port='5439' user='" + config.RedshiftWappa['username'] + "' password='" +  config.RedshiftWappa['password'] + "' host='" + config.RedshiftWappa['host']+ "' connect_timeout=3000 "


def listareventosumarizado():
    con = psycopg2.connect(conn_string)
    list = []
    cur = con.cursor()


    cur.execute( constant.sqleventosredshift)
    for result in cur:
        retorno = "{}|{}|{}|{}|{}|{}\n".format(result[0], result[1], result[2],result[3], result[4], result[5])
        list.append(retorno)

    return list


def listareventosumarizadodriver():
    con = psycopg2.connect(conn_string)
    list = []
    cur = con.cursor()

    cur.execute(constant.sqleventosredshiftdriver)
    for result in cur:
        retorno = "{}|{}|{}|{}\n".format(result[0], result[1], result[2], result[3])
        list.append(retorno)

    return list
