import uuid
import sys
import datetime
import json

globalCorrelationId = ''

def generateNewCorrelationId():
    global globalCorrelationId
    globalCorrelationId = str(uuid.uuid4())

generateNewCorrelationId()


def log(descricaomessagem, level, type, appdomain, exception=""):
    mensagem = '{Date}|{appdomain}:{correlationid}|{type}|{level}|{message}|{exception}'.format(
        Date=datetime.datetime.now(),
        appdomain=appdomain,
        correlationid=globalCorrelationId,
        type=type,
        level=level,
        message=descricaomessagem,
        exception=exception)

    print(mensagem)
    sys.stdout.flush()


def info(eventName, info=None):
    event = {
        'eventName': eventName
    }

    if (info != None):
        event['info'] = info

    log(json.dumps(event), "INFO", "LOG", "Wappa.Sync.RedShiftPostgree")


def error(eventName, exception):
    event = {
        'eventName': eventName,
    }

    log(json.dumps(event), "ERROR", "LOG", "Wappa.Sync.RedShiftPostgree", exception)
