import config
import psycopg2
import constant

conn_string = "dbname='" + config.PostGreeWappa['database'] + "' port='"+config.PostGreeWappa['port']+"' user='" + config.PostGreeWappa['username'] + "' password='" +  config.PostGreeWappa['password'] + "' host='" + config.PostGreeWappa['host']+ "' connect_timeout=3000 "


def importcsv(filename) :
    con = psycopg2.connect(conn_string)

    cur = con.cursor()
    cur.execute(constant.sqltruncatetemp)
    con.commit()

    with open(filename, 'r',  encoding='utf-8') as f:
        cur.copy_from(f, 'base.driver_ride_score_detail_temp', sep='|', null ='' , columns=('driverid', 'driveracceptedride', 'driverarrivedatpickup', 'driverrescindedride',
                                                                                            'drideofferwithdrawn', 'driverrejectedride' ))

    con.commit()

    cur.execute(constant.sqldeleteevent)
    con.commit()

    cur.execute(constant.sqlupdateevent)
    con.commit()

    cur.execute(constant.sqlinsertevent )
    con.commit()
    con.close()

def importcsvdriverevent(filename) :
    con = psycopg2.connect(conn_string)

    cur = con.cursor()
    cur.execute(constant.sqltruncatetempdriver)
    con.commit()

    with open(filename, 'r',  encoding='utf-8') as f:
        cur.copy_from(f, 'base.driver_event_temp', sep='|', null ='' , columns=('driverid', 'event', 'reason', 'quantity' ))

    con.commit()

    cur.execute(constant.sqldeleteeventdriver)
    con.commit()

    cur.execute(constant.sqlupdateeventcriver   )
    con.commit()

    cur.execute(constant.sqlinserteventdriver )
    con.commit()
    con.close()